# WGS Indel Analyses

Analyses relating to mouse tumour, RPE1 bottlenck and de novo indel variants

## INSTALLATION/USAGE

To run these notebooks you will need [jupyter](https://jupyter.org/) (either
jupyter notebook or jupyter lab) installed with a python3 kernel. The following
python modules are also required:

* pysam
* pyfaidx
* pandas
* numpy
* scipy
* scikit-learn
* matplotlib
* seaborn
* logomaker
* indel_repeat_classifier (https://github.com/david-a-parry/indel_repeat_classifier)
* region_finder (https://github.com/david-a-parry/region_finder)

These can all be installed via pip, as per the example below:

~~~
# install all those available via PyPi
python3 -m pip install pysam pyfaidx pandas numpy scipy scikit-learn matplotlib seaborn logomaker --user

# install those available via github
python3 -m pip install git+git://github.com/david-a-parry/indel_repeat_classifier.git --user
python3 -m pip install git+git://github.com/david-a-parry/region_finder.git --user
~~~

Once you have the jupyter installed and the required python modules you may
clone this repository and launch jupyter:

~~~
git clone --recursive https://git.ecdf.ed.ac.uk/Deletions_paper/wgs_variant_analyses.git
cd wgs_variant_analyses
jupyter-notebook
~~~

## DATA

Gene4Denovo data is publically available and can be downloaded by running the
relevant cells in the notebook '01_variants_to_tables'. The variant data that
underlie the mouse tumour and RPE1 cell analysis will be made available in due
course, but for the time being the code and cell-outputs for these analyses are
provided for inspection.

## AUTHOR

Written by David A. Parry at the University of Edinburgh.
