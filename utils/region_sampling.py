import pandas as pd
from region_finder.bed_parser import BedParser
from region_finder.interval_sampler import IntervalSampler
from pyfaidx import Fasta
from collections import namedtuple
from indel_repeat_classifier.repeats_from_variants import repeats_from_variant
from indel_repeat_classifier.repeats_from_variants import repeat_result_to_ID83
from .utils import repeat_type_from_cosmic_class

Variant = namedtuple("Variant", "chrom pos ref alts alleles start end")
v_attrs = ('chrom', 'pos', 'ref')
res_attrs = ('variant_type', 'repeat_type', 'repeat_unit', 'repeat_length',
             'sequence')


def left_align_interval(contig, start, end, fasta, verbose=False):
    '''
    Shift interval to left most position given sequence ambiguity. This is
    equivalent to left-aligning of indel variant calls.

    Args:
        contig: contig/chromosome/sequence name for interval

        start:  0-based start position

        end:    1-based end position

        fasta:  pyfaidx Fasta object containing sequence (assumed to be all
                uppercase using the always_upper=True option).

    '''
    seq = fasta[contig]
    while seq[start - 1] == seq[end - 1] and start > 0:
        # if base immediately preceding interval == last base of deletion
        # shift back by one
        if verbose:
            print("{}-{}: {}".format(
                start, end, seq[start - 10:start].lower() + seq[start:end] +
                seq[end:end + 10].lower()))
        start -= 1
        end -= 1
    if verbose:
        print("{}-{}: {}".format(
            start, end, seq[start - 10:start].lower() + seq[start:end] +
            seq[end:end + 10].lower()))
    return start, end


def random_seqs_from_bed(bed, fasta, lengths, debug=False):
    annot_cols = v_attrs + ('alt', ) + res_attrs + (
        'variant_length', 'cosmic_class', 'cosmic_repeat_type')
    if isinstance(bed, str):
        bedparser = BedParser(bed)
    elif isinstance(bed, BedParser):
        bedparser = bed
    elif isinstance(bed, IntervalSampler):
        bedparser = None
        sampler = bed
    else:
        raise ValueError("Unsupported type for 'bed' argument: {}".format(
            type(bed)))
    if bedparser is not None:
        sampler = IntervalSampler(bedparser)
    assert sampler is not None
    rand_sample = sampler.random_sample_given_lengths(lengths,
                                                      guarantee_lengths=True)
    if isinstance(fasta, str):
        faidx = Fasta(fasta, as_raw=True, sequence_always_upper=True)
    elif isinstance(fasta, Fasta):
        faidx = fasta
    else:
        raise ValueError("Unsupported type for 'fasta' argument: {}".format(
            type(fasta)))
    records = []
    for intvl in rand_sample:
        if debug:
            print("Interval = {}".format(intvl))
        row = []
        start, end = left_align_interval(str(intvl.contig),
                                         intvl.start,
                                         intvl.end,
                                         faidx,
                                         verbose=debug)
        if start < 1:
            start = 1
            end = 1 + intvl.end - intvl.start
        if debug:
            print("Left aligned = {}-{}".format(start, end))
        ref = faidx[intvl.contig][start - 1:end]
        alt = faidx[intvl.contig][start - 1]
        alleles = [ref, alt]
        variant = Variant(intvl.contig, start, ref, [alt], alleles, start - 1,
                          start - 1 + len(ref))
        if debug:
            print(variant)
        rpt_res = repeats_from_variant(variant=variant, fasta=faidx)
        for attr in v_attrs:
            row.append(getattr(variant, attr))
        row.append(variant.alts[0])
        for attr in res_attrs:
            row.append(getattr(rpt_res, attr))
        variant_length = abs(len(variant.alts[0]) - len(variant.ref))
        cosmic_class = repeat_result_to_ID83(rpt_res,
                                             variant=variant,
                                             fasta=faidx,
                                             allele=1)
        cosmic_repeat_type = repeat_type_from_cosmic_class(cosmic_class)
        row.extend([variant_length, cosmic_class, cosmic_repeat_type])
        records.append(row)
    return pd.DataFrame.from_records(records, columns=annot_cols)