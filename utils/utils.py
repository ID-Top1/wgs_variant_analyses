'''
Functions and variables shared by jupyter-notebooks in
variants_and_repeats_analysis
'''

from collections import defaultdict
import pandas as pd

id83_colors = [[253 / 256, 190 / 256, 111 / 256],
               [255 / 256, 128 / 256, 2 / 256],
               [176 / 256, 221 / 256, 139 / 256],
               [54 / 256, 161 / 256, 46 / 256],
               [253 / 256, 202 / 256, 181 / 256],
               [252 / 256, 138 / 256, 106 / 256],
               [241 / 256, 68 / 256, 50 / 256],
               [188 / 256, 25 / 256, 26 / 256],
               [208 / 256, 225 / 256, 242 / 256],
               [148 / 256, 196 / 256, 223 / 256],
               [74 / 256, 152 / 256, 201 / 256],
               [23 / 256, 100 / 256, 171 / 256],
               [226 / 256, 226 / 256, 239 / 256],
               [182 / 256, 182 / 256, 216 / 256],
               [134 / 256, 131 / 256, 189 / 256],
               [98 / 256, 64 / 256, 155 / 256]]

two_bp_cats = {
    'AA': 'AA-AT',
    'TT': 'AA-AT',
    'AT': 'AA-AT',
    'TA': 'AA-AT',
    'CT': 'CT-CA',
    'TC': 'CT-CA',
    'AC': 'CT-CA',
    'CA': 'CT-CA',
    'GA': 'CT-CA',
    'AG': 'CT-CA',
    'GT': 'CT-CA',
    'TG': 'CT-CA',
    'CC': 'CC-CG',
    'GG': 'CC-CG',
    'CG': 'CC-CG',
    'GC': 'CC-CG',
}

two_bp_rpt_order_all = [
    'AA-AT not in repeat', 'CT-CA not in repeat', 'CC-CG not in repeat',
    'AA-AT imperfect repeat', 'CT-CA imperfect repeat',
    'CC-CG imperfect repeat', 'AA-AT short repeat', 'CT-CA short repeat',
    'CC-CG short repeat', 'AA-AT long repeat', 'CT-CA long repeat',
    'CC-CG long repeat'
]

two_bp_rpt_order = [
    'AA-AT not in repeat', 'CT-CA not in repeat', 'AA-AT imperfect repeat',
    'CT-CA imperfect repeat', 'AA-AT short repeat', 'CT-CA short repeat',
    'AA-AT long repeat', 'CT-CA long repeat', 'other'
]

comp = str.maketrans('acgtACGT', 'tgcaTGCA')


def complement(seq):
    return seq.translate(comp)


def revcomp(seq):
    return complement(seq)[::-1]


def get_id83_pal():
    ''' Return a list of colors corresponding to ID83 plots'''
    id83_pal = []
    for i in range(12):
        for j in range(6):
            id83_pal.append(id83_colors[i])
    for j in range(2, 6):
        i += 1
        for k in range(1, j):
            id83_pal.append(id83_colors[i])
    id83_pal.append(id83_colors[i])
    return id83_pal


def get_id83_order():
    '''
    Return a list of ID83 mutation types in the order expected by SigProfiler
    '''
    i_order = []
    variant_types = ('Del', 'Ins')
    for vt in variant_types:
        for nt in ['C', 'T']:
            for i in range(6):
                i_order.append('1:{}:{}:{}'.format(vt, nt, i))
    for vt in variant_types:
        for i in range(2, 6):
            for j in range(6):
                i_order.append('{}:{}:R:{}'.format(i, vt, j))
    for i in range(2, 6):
        for j in range(1, i):
            i_order.append('{}:Del:M:{}'.format(i, j))
    i_order.append('5:Del:M:5')
    return i_order


def df_to_group_counts(df, cols):
    '''
    For given datafrane abd columns produce a new dataframe of value counts
    with non-existent subcategories filled with zeroes
    '''
    cdf = df.groupby(cols)[cols[0]].count().to_frame()
    cdf.columns = ['Count']
    return cdf.unstack().fillna(0).stack().reset_index()


def simplify_indel(ref, alt, pos):
    '''
    Trim ref/alt alleles to their simplest representations. Identical suffxies
    and prefixes are removed and the position shifted accordingly.
    '''
    while len(ref) > 1 and len(alt) > 1:
        if ref[-1] == alt[-1]:
            ref = ref[:-1]
            alt = alt[:-1]
        else:
            break
    while len(ref) > 1 and len(alt) > 1:
        if ref[0] == alt[0]:
            ref = ref[1:]
            alt = alt[1:]
            pos += 1
        else:
            break
    return ref, alt, pos


def cosmic_repeat_length_cat(x):
    ln = int(x.split(':')[-1])
    if ln >= 5:
        return 'Long'
    if int(x.split(':')[-1]) > 0:
        return 'Short'
    return 'No repeat'


def repeat_type_from_cosmic_class(c):
    vlen, vtype, rtype, rlen = c.split(':')
    if rlen == '0':
        return 'No repeat'
    if rtype == 'M':
        return 'Imperfect'
    return 'Perfect'


def tnt_compliance_from_row(row):
    '''
    A deletion is TnT compliant if the deleted nucleotides (given uncertainty of
    exact deletion site at repeats) match the pattern NT with a T immediately 5'
    of the deleted nucleotides (i.e. T|NT|N). Therefore, any deletion at a
    perfect repeat (STR) with A/T in the repeat unit (e.g. CTCT) is compliant
    with the TnT motif. For deletions at microhomology sites, if A/T is in the
    microhomology region (e.g. TCT), the repeat is TnT compliant.
    '''
    if row.variant_type != 'Del' or row.repeat_type == 'No repeat':
        return False
    if row.repeat_type == 'Perfect':
        return 'A' in row.repeat_unit or 'T' in row.repeat_unit
    mh_len = row.repeat_length - row.variant_length
    mh_seq = row.repeat_unit[:mh_len]
    return 'A' in mh_seq or 'T' in mh_seq


def read_rpt_files(rpt_files):
    '''
    Read files of repeat type counts generated as detailed in
    04_deleted_dinucleotide_sequence_analysis.ipynb
    '''
    tmp_counts = defaultdict(int)
    counts = defaultdict(list)
    for txt in rpt_files:
        with open(txt, 'rt') as fh:
            for line in fh:
                count, rpt = line.split()
                tmp_counts[rpt] += int(count)
    for k, v in tmp_counts.items():
        rpt_type, rpt_len, rpt_unit = k.split(',')
        counts['repeat_type'].append(rpt_type)
        counts['repeat_length'].append(int(rpt_len))
        counts['repeat_unit'].append(rpt_unit)
        counts['Count'].append(v)
    counts = pd.DataFrame.from_dict(counts)
    counts['variant_length'] = counts.repeat_unit.apply(lambda x: len(x))
    counts['variant_type'] = 'Del'
    counts['repeat_length_cat'] = counts.repeat_length.apply(
        lambda x: str(x) if x < 5 else ">4")
    counts['N_repeats'] = counts.apply(lambda x: 1
                                       if x.repeat_type == 'Imperfect' else x.
                                       repeat_length // x.variant_length - 1,
                                       axis=1)
    counts['TNT_compliant'] = counts.apply(
        lambda x: tnt_compliance_from_row(x), axis=1)
    counts['total_bp'] = counts.repeat_length * counts.Count
    return counts
