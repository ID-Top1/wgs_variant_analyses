This file was produced by vcfisec.
The command line was:	bcftools isec  -p variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4 -n +3 -f PASS mutect2_calls/mutect2_somatic_calls/AKO1_4/AKO1_4.somatic.filtered_no_contam.vcf.gz strelka_somatic_calls/strelka_calls/AKO1_4/results/variants/somatic.indels.vcf.gz strelka_somatic_calls/strelka_calls/AKO1_4/results/variants/somatic.snvs.vcf.gz svaba_somatic_indels/AKO1_4/AKO1_4.svaba.somatic.indel.renamed.norm.vcf.gz platypus_output/per_sample_somatic/AKO1_4.norm.ad2_pass.vaf_ratio_10.vcf.gz pindel_output/AKO1_4/AKO1_4.pindel.norm.ad2.vcf.gz

Using the following file names:
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4/0000.vcf	for stripped	mutect2_calls/mutect2_somatic_calls/AKO1_4/AKO1_4.somatic.filtered_no_contam.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4/0001.vcf	for stripped	strelka_somatic_calls/strelka_calls/AKO1_4/results/variants/somatic.indels.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4/0002.vcf	for stripped	strelka_somatic_calls/strelka_calls/AKO1_4/results/variants/somatic.snvs.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4/0003.vcf	for stripped	svaba_somatic_indels/AKO1_4/AKO1_4.svaba.somatic.indel.renamed.norm.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4/0004.vcf	for stripped	platypus_output/per_sample_somatic/AKO1_4.norm.ad2_pass.vaf_ratio_10.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/AKO1_4/0005.vcf	for stripped	pindel_output/AKO1_4/AKO1_4.pindel.norm.ad2.vcf.gz
