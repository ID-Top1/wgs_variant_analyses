This file was produced by vcfisec.
The command line was:	bcftools isec  -p variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1 -n +3 -f PASS mutect2_calls/mutect2_somatic_calls/WT1_1/WT1_1.somatic.filtered_no_contam.vcf.gz strelka_somatic_calls/strelka_calls/WT1_1/results/variants/somatic.indels.vcf.gz strelka_somatic_calls/strelka_calls/WT1_1/results/variants/somatic.snvs.vcf.gz svaba_somatic_indels/WT1_1/WT1_1.svaba.somatic.indel.renamed.norm.vcf.gz platypus_output/per_sample_somatic/WT1_1.norm.ad2_pass.vaf_ratio_10.vcf.gz pindel_output/WT1_1/WT1_1.pindel.norm.ad2.vcf.gz

Using the following file names:
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1/0000.vcf	for stripped	mutect2_calls/mutect2_somatic_calls/WT1_1/WT1_1.somatic.filtered_no_contam.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1/0001.vcf	for stripped	strelka_somatic_calls/strelka_calls/WT1_1/results/variants/somatic.indels.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1/0002.vcf	for stripped	strelka_somatic_calls/strelka_calls/WT1_1/results/variants/somatic.snvs.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1/0003.vcf	for stripped	svaba_somatic_indels/WT1_1/WT1_1.svaba.somatic.indel.renamed.norm.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1/0004.vcf	for stripped	platypus_output/per_sample_somatic/WT1_1.norm.ad2_pass.vaf_ratio_10.vcf.gz
variant_intersections/min_3_pass_intersections/all_intersected_vcfs/WT1_1/0005.vcf	for stripped	pindel_output/WT1_1/WT1_1.pindel.norm.ad2.vcf.gz
