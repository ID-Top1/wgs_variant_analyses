This file was produced by vcfisec.
The command line was:	bcftools isec  -p variant_intersections/min_3_intersections/all_intersected_vcfs/A20.1 -n +3 -f PASS mutect2_somatic_calls/A20.1/A20.1.somatic.filtered_no_contam.vcf.gz strelka_calls/A20.1/results/variants/somatic.indels.vcf.gz strelka_calls/A20.1/results/variants/somatic.snvs.vcf.gz svaba_somatic_indels/A20.1/A20.1.svaba.somatic.indel.renamed.norm.vcf.gz platypus_output/per_sample_somatic/A20.1.norm.ad2_pass.vaf_ratio_10.vcf.gz

Using the following file names:
variant_intersections/min_3_intersections/all_intersected_vcfs/A20.1/0000.vcf	for stripped	mutect2_somatic_calls/A20.1/A20.1.somatic.filtered_no_contam.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/A20.1/0001.vcf	for stripped	strelka_calls/A20.1/results/variants/somatic.indels.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/A20.1/0002.vcf	for stripped	strelka_calls/A20.1/results/variants/somatic.snvs.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/A20.1/0003.vcf	for stripped	svaba_somatic_indels/A20.1/A20.1.svaba.somatic.indel.renamed.norm.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/A20.1/0004.vcf	for stripped	platypus_output/per_sample_somatic/A20.1.norm.ad2_pass.vaf_ratio_10.vcf.gz
