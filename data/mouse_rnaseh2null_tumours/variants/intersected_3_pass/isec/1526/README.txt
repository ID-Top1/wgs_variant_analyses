This file was produced by vcfisec.
The command line was:	bcftools isec  -p variant_intersections/min_3_intersections/all_intersected_vcfs/1526 -n +3 -f PASS mutect2_somatic_calls/1526/1526.somatic.filtered_no_contam.vcf.gz strelka_calls/1526/results/variants/somatic.indels.vcf.gz strelka_calls/1526/results/variants/somatic.snvs.vcf.gz svaba_somatic_indels/1526/1526.svaba.somatic.indel.renamed.norm.vcf.gz platypus_output/per_sample_somatic/1526.norm.ad2_pass.vaf_ratio_10.vcf.gz

Using the following file names:
variant_intersections/min_3_intersections/all_intersected_vcfs/1526/0000.vcf	for stripped	mutect2_somatic_calls/1526/1526.somatic.filtered_no_contam.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/1526/0001.vcf	for stripped	strelka_calls/1526/results/variants/somatic.indels.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/1526/0002.vcf	for stripped	strelka_calls/1526/results/variants/somatic.snvs.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/1526/0003.vcf	for stripped	svaba_somatic_indels/1526/1526.svaba.somatic.indel.renamed.norm.vcf.gz
variant_intersections/min_3_intersections/all_intersected_vcfs/1526/0004.vcf	for stripped	platypus_output/per_sample_somatic/1526.norm.ad2_pass.vaf_ratio_10.vcf.gz
